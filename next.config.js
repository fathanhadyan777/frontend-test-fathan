const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = {
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: 'https://suitmedia-backend.suitdev.com/api/:path*', // Proxy to Backend
      },
    ];
  },
  webpack: (config, { dev, isServer }) => {
    if (dev && !isServer) {
      // Enable proxying in development mode
      config.devServer = config.devServer || {};
      config.devServer.proxy = {
        '/api': {
          target: 'https://suitmedia-backend.suitdev.com',
          changeOrigin: true,
          pathRewrite: { '^/api': '' },
          secure: false,
        },
      };
    }
    return config;
  },
};
