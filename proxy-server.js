const express = require('express');
const axios = require('axios');
const http = require('http'); // Menggunakan http module

const app = express();
const proxy = 'http://localhost:3001'; // Proxy server configuration

const params = {
  'page[number]': 1,
  'page[size]': 10,
  'append[]': ['small_image', 'medium_image'],
  'sort': '-published_at'
};

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/proxy', async (req, res) => {
  try {
    const response = await axios.get('https://suitmedia-backend.suitdev.com/api/ideas', {
      params: params,
      httpAgent: new http.Agent({ keepAlive: true }), // Menggunakan http agent
      headers: {
        'Accept': 'application/json',
        'User-Agent': 'Mozilla/5.0'
      }
    });

    res.send(response.data);
  } catch (error) {
    console.error('Error fetching data:', error);
    res.status(500).send('Internal Server Error');
  }
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Proxy server is running on port ${PORT}`);
});
