"use client"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../styles/globals.css';
import styles from './PostList.module.css';

interface Image {
  url: string;
}

interface Post {
  proxiedImageUrl: string | undefined;
  id: number;
  title: string;
  published_at: string;
  small_image?: Image[];
  medium_image?: Image[];
}

const PostList: React.FC = () => {
  const [posts, setPosts] = useState<Post[]>([]);
  const [itemsPerPage, setItemsPerPage] = useState<number>(() => {
    if (typeof localStorage !== 'undefined') {
      return parseInt(localStorage.getItem('itemsPerPage') || '10', 10);
    }
    return 10;
  });
  const [currentPage, setCurrentPage] = useState<number>(() => {
    if (typeof localStorage !== 'undefined') {
      return parseInt(localStorage.getItem('currentPage') || '1', 10);
    }
    return 1;
  });
  const [sortOrder, setSortOrder] = useState<string>(() => {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem('sortOrder') || '-published_at';
    }
    return '-published_at';
  });
  const [totalPages, setTotalPages] = useState(0);
  const [pageRange, setPageRange] = useState<{ start: number; end: number }>({
    start: 1,
    end: 5,
  });

  useEffect(() => {
    const fetchData = async () => {
      const url = `https://suitmedia-backend.suitdev.com/api/ideas`;
      const params = {
        'page[number]': currentPage,
        'page[size]': itemsPerPage,
        'append[]': ['small_image', 'medium_image'],
        'sort': sortOrder
      };

      console.log("Fetching URL:", url);
      console.log("Params:", params);

      try {
        const res = await axios.get(url, {
          params: params,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        });

        const data = res.data;
        console.log("Response Data:", data);

        if (data && data.data) {
          const postsWithImages = await Promise.all(
            data.data.map(async (post: Post) => {
              const imageUrl = post.small_image?.[0]?.url || post.medium_image?.[0]?.url || '';
              let proxiedImageUrl = `http://localhost:3001/proxy?url=${encodeURIComponent(imageUrl)}`;
              console.log("Proxied Image URL:", proxiedImageUrl);
              return { ...post, proxiedImageUrl };
            })
          );
          setPosts(prevPosts => [...prevPosts, ...postsWithImages]);
          setTotalPages(data.meta?.last_page || 0);
        } else {
          console.error('Unexpected data format:', data);
        }
      } catch (error) {
        console.error('Fetch error:', error);
      }
    };

    fetchData();
  }, [currentPage, itemsPerPage, sortOrder]);

  useEffect(() => {
    if (typeof localStorage !== 'undefined') {
      localStorage.setItem('itemsPerPage', itemsPerPage.toString());
      localStorage.setItem('currentPage', currentPage.toString());
      localStorage.setItem('sortOrder', sortOrder);
    }
  }, [itemsPerPage, currentPage, sortOrder]);

  const handleItemsPerPageChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setItemsPerPage(parseInt(e.target.value, 10));
    setCurrentPage(1);
    setPageRange({ start: 1, end: 5 });
  };

  const handleSortOrderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSortOrder(e.target.value);
  };

  const handleNextPage = () => {
    setCurrentPage((prevPage) => {
      const nextPage = Math.min(prevPage + 1, totalPages);
      const nextStart = nextPage - ((nextPage - 1) % 5);
      const nextEnd = Math.min(nextStart + 4, totalPages);
      setPageRange({ start: nextStart, end: nextEnd });
      return nextPage;
    });
  };

  const handlePrevPage = () => {
    setCurrentPage((prevPage) => {
      const prevPageNumber = Math.max(prevPage - 1, 1);
      const prevStart = prevPageNumber - ((prevPageNumber - 1) % 5);
      const prevEnd = Math.min(prevStart + 4, totalPages);
      setPageRange({ start: prevStart, end: prevEnd });
      return prevPageNumber;
    });
  };

  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber);
    const start = pageNumber - ((pageNumber - 1) % 5);
    const end = Math.min(start + 4, totalPages);
    setPageRange({ start, end });
  };

  const renderPageNumbers = () => {
    const pageNumbers = [];
    for (let i = pageRange.start; i <= pageRange.end; i++) {
      pageNumbers.push(
        <button
          key={i}
          onClick={() => paginate(i)}
          className={`px-3 py-1 mx-1 ${
            currentPage === i
              ? "bg-blue-700 text-white"
              : "bg-white border border-[#DFE4EA] text-[#637381] hover:bg-[#A8D4FF] hover:text-white"
          } rounded`}
        >
          {i}
        </button>
      );
    }
    return pageNumbers;
  };

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const displayedPosts = posts.slice(indexOfFirstItem, indexOfLastItem);

  return (
    <div>
      <div className="flex mb-4">
        <p className="pt-1 text-m">Show per page:</p>
        <div className={styles.dropdownContainer}>
          <select
            value={itemsPerPage}
            onChange={handleItemsPerPageChange}
            className={`${styles.dropdown} dropdown`}
          >
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
        </div>
        <p className="pt-1 pl-4 text-m">Sort By:</p>
        <div className={styles.dropdownContainer}>
          <select
            value={sortOrder}
            onChange={handleSortOrderChange}
            className={`${styles.dropdown} dropdown`}
          >
            <option value="-published_at">Newest</option>
            <option value="published_at">Oldest</option>
          </select>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        {displayedPosts.map(post => (
          <div className="border p-4" key={post.id}>
            <img
              src={post.proxiedImageUrl}
              alt={post.title}
              className="w-full h-auto"
              loading="lazy"
              style={{ aspectRatio: '16/9' }}
            />
            <h2 className="text-xl font-bold mt-2 truncate" style={{ display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical', overflow: 'hidden' }}>{post.title}</h2>
            <p>{new Date(post.published_at).toLocaleDateString()}</p>
          </div>
        ))}
      </div>
      <div className="flex justify-center my-4">
        <div className={`${styles.pagination_container} p-2`}>
          <button
            onClick={handlePrevPage}
            disabled={currentPage === 1}
            className="px-3 py-1 mx-1 bg-white border border-[#DFE4EA] text-[#637381] rounded hover:bg-[#A8D4FF] hover:text-white"
          >
            {"<"}
          </button>
          {renderPageNumbers()}
          <button
            onClick={handleNextPage}
            disabled={currentPage === totalPages}
            className="px-3 py-1 mx-1 bg-white border border-[#DFE4EA] text-[#637381] rounded hover:bg-[#A8D4FF] hover:text-white"
          >
            {">"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default PostList;
