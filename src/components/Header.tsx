// src/components/Header.tsx
"use client";
import { useEffect, useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import { useRouter } from 'next/navigation';
import styles from "./Header.module.css";
import { InterReguler } from "../font/font";
import Image from "next/image";
import logo from '../../public/images/logo.png';

const Header: React.FC = () => {
  const [show, setShow] = useState(true);
  const [navbar, setNavbar] = useState(false);
  const [lastScrollY, setLastScrollY] = useState(0);
  const router = useRouter();
  const [activeItem, setActiveItem] = useState<string | null>(null);

  const menuItems = [
    { name: "Work", path: "/under-dev" },
    { name: "About", path: "/under-dev" },
    { name: "Services", path: "/under-dev" },
    { name: "Ideas", path: "/" },
    { name: "Careers", path: "/under-dev" },
    { name: "Contact", path: "/under-dev" },
  ];

  const controlHeader = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY > lastScrollY) {
        setShow(false);
      } else {
        setShow(true);
      }
      setLastScrollY(window.scrollY);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", controlHeader);
      return () => {
        window.removeEventListener("scroll", controlHeader);
      };
    }
  }, [lastScrollY]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
    const path = window.location.pathname;
    const currentItem = menuItems.find(item => item.path === path);
    setActiveItem(currentItem ? currentItem.name : null);
    }
  }, [typeof window !== 'undefined' ? window.location.pathname : '']);

  const handleItemClick = (name: string, path: string) => {
    setActiveItem(name);
    router.push(path);
  };

  return (
    <div>
      <nav className={`fixed w-full z-50 ${show ? "bg-orange-500" : "bg-transparent"} transition-all duration-300 ease-in-out`}>
        <div className="mx-auto px-4">
          <div className="md:flex md:justify-between md:items-center">
            <div className="flex justify-between items-stretch gap-x-4">
              <a href="/" className="flex items-center py-4 pl-8">
                <Image src={logo} alt="Logo" width={140} height={50} />
              </a>
              <span className="md:hidden block">
                {navbar ? (
                  <button
                    aria-label="close icon"
                    className={`p-2 rounded-md ${styles.navbar_logo}`}
                    onClick={() => setNavbar(!navbar)}
                  >
                    <FaTimes />
                  </button>
                ) : (
                  <button
                    aria-label="drawer icon"
                    className={`p-2 rounded-md ${styles.navbar_logo}`}
                    onClick={() => setNavbar(!navbar)}
                  >
                    <FaBars />
                  </button>
                )}
              </span>
            </div>
            <ul
              className={`md:flex md:justify-end md:items-center md:space-x-2 md:pr-20 text-[16px] right-7 absolute md:static md:z-auto z-[1] ${styles.nav_items_tx} ${navbar ? `top-[80px] ${styles.card} w-28` : "top-[-490px]"}`}
              style={InterReguler.style}
            >
              {menuItems.map((item, index) => (
                <li
                  key={index}
                  className={`md:border-0 border-b-[1px] p-2 ${
                    activeItem === item.name
                      ? "underline underline-offset-4 decoration-white"
                      : "hover:bg-orange-500 md:hover:bg-transparent"
                  } ${activeItem === item.name ? "text-white" : "text-white md:hover:text-primaryForeground"}`}
                >
                  <a href="#" onClick={(e) => {
                    e.preventDefault();
                    handleItemClick(item.name, item.path);
                  }}>
                    {item.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
