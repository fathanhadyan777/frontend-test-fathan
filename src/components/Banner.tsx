"use client";
import { useEffect, useRef } from "react";
import "../styles/globals.css";

interface BannerProps {
  imageUrl: string;
  children: React.ReactNode;
}

const Banner: React.FC<BannerProps> = ({ imageUrl, children }) => {
  const bannerRef = useRef<HTMLDivElement>(null);
  const textRef = useRef<HTMLDivElement>(null);

  const handleScroll = () => {
    const scrollTop = window.pageYOffset;
    if (bannerRef.current) {
      // Menambahkan efek parallax untuk background image
      const backgroundPositionY = `${scrollTop * 0.5}px`;
      bannerRef.current.style.backgroundPositionY = backgroundPositionY;
    }
    if (textRef.current) {
      // Menambahkan efek parallax untuk teks
      const translateY = `${scrollTop * 0.3}px`;
      textRef.current.style.transform = `translateY(${translateY})`;
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div
      ref={bannerRef}
      className="relative h-80 bg-cover bg-fixed flex items-center justify-center text-white overflow-hidden"
      style={{ backgroundImage: `url(${imageUrl})` }}
    >
      {/* Background image */}
      <div className="absolute inset-0 bg-cover bg-center" style={{ backgroundImage: `url(${imageUrl})` }}></div>
      
      {/* Efek miring pada bagian bawah banner dengan transform */}
      <div
        className="absolute bottom-0 left-0 w-full h-20 bg-white"
        style={{
          transform: "skewY(-3deg)", // Miringkan bagian bawah dengan sudut -10 derajat
          transformOrigin: "bottom right",
        }}
      >
        <div className="w-full h-full bg-white opacity-50"></div>
      </div>

      {/* Konten teks dengan efek parallax */}
      <div className="text-center z-10 relative" ref={textRef}>
        {children}
      </div>
    </div>
  );
};

export default Banner;
