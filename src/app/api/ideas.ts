// src/pages/api/ideas.ts
import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { page, size, sort } = req.query;

  const params = new URLSearchParams({
    'page[number]': page?.toString() || '1',
    'page[size]': size?.toString() || '10',
    'sort': sort?.toString() || '-published_at',
  });

  // Hardcode append parameters
  params.append('append[]', 'small_image');
  params.append('append[]', 'medium_image');

  try {
    const response = await fetch(`https://suitmedia-backend.suitdev.com/api/ideas?${params.toString()}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    });

    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch data' });
  }
}
