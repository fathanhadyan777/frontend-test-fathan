// pages/under-development.js
import React from 'react';
import Header from '../../components/Header';
import '../../styles/globals.css';


const UnderDevelopment = () => {
  return (
    <><Header />
    <div className="flex items-center justify-center h-screen bg-gray-100">
      <div className="text-center p-4">
        <h1 className="text-4xl font-bold mb-4">Page Under Development</h1>
        <p className="text-lg">This page is currently under development. Please check back later.</p>
      </div>
    </div></>
  );
};

export default UnderDevelopment;
