// src/app/page.tsx
import Header from '../components/Header';
import Banner from '../components/Banner';
import Postlist from '../components/PostList';
import '../styles/globals.css';

const Home: React.FC = () => {
  return (
    <>
      <Header/>
      <Banner imageUrl="/images/banner.jpg">
        <h1 className="text-4xl font-bold">Ideas</h1>
        <p className="text-xl">Where all our great things begin</p>
      </Banner>
      <div className="container mx-auto py-8">
        <Postlist />
      </div>
    </>
  );
};

export default Home;
