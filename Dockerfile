# Gunakan image Node.js sebagai base image
FROM node:21-alpine

# Set direktori kerja
WORKDIR /usr/src/app

# Salin file package.json dan package-lock.json
COPY package*.json ./

# Install dependensi
RUN npm install

# Salin file aplikasi
COPY . .

# Build aplikasi
RUN npm run build

# Tentukan port yang akan digunakan oleh aplikasi
EXPOSE 3000

# Tentukan perintah untuk menjalankan aplikasi
CMD ["npm", "start"]
